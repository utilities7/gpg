
FROM alpine:3.6
RUN apk --update add \
   bash \
   gnupg \
   openssh \
   openssl \
   && rm -rf /var/cache/apk/*


RUN adduser -D -g '' gitcrypt
USER gitcrypt

WORKDIR /repo
VOLUME /repo
VOLUME /home/gitcrypt
ENTRYPOINT ["gpg"]
